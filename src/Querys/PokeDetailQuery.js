export function POKEMON_DETAIL_QUERY(name){
  return `query {
    Pokemon(name: "${name}") {
    id
    name
    image
    abilities {
      name
    }
    stats {
      name
      value
    }
    types {
      name
    }
  }
  }
  `;
} 
