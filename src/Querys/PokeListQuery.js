export const POKELIST_QUERY = `query {
  Pokemons(first: 151) {
    id
    name
  }
}
`
