export function POKEMON_CARD_QUERY(name){
  return `query {
    Pokemon(name: "${name}") {
    id
    name
    image
    types {
      name
    }
    abilities {
      name
    }
    }
  }
  `;
} 
