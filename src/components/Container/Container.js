
import styles from './container.module.scss';

export default function Container(props) {
  return <div {...props} className={styles.root} />;
}
