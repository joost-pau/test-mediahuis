import React from 'react'
import { useQuery } from 'graphql-hooks'
import { POKEMON_DETAIL_QUERY } from '../../Querys/PokeDetailQuery';
import Pokestats from '../Pokestats/Pokestats';
import SelectedMoves from '../SelectedMoves/SelectedMoves';
import AbilitiesList from '../AbilitiesList/AbilitiesList';
import styles from './PokeDetail.module.scss'

export default function PokeDetail(props) {
  const { loading, data } = useQuery(POKEMON_DETAIL_QUERY(props.DetailPokemon));
  if (loading) return 'Loading...'
  const pokemon = data.Pokemon;
  // 
  return (
    <div className={styles.pokedetail__wrapper}>
      <div className="pokedetail__img_wrapper">
        <img src={pokemon.image} alt={pokemon.name}  className={styles.pokeImg}/>
        <p className={styles.pokeName}>{pokemon.name}</p>
        <button className={styles.btn__prim} onClick={props.AddPokemon}>Save Pokemon</button>
      </div>
      <div className={styles.pokedetail__info_wrapper}>
        <Pokestats stats={pokemon.stats}/>
        <SelectedMoves/>
      </div>
      <AbilitiesList abilities={pokemon.abilities}/>
    </div>
  )
}

