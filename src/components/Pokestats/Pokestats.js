import React from 'react';
import styles from './Pokestats.module.scss';
import Stat from '../Stat/Stat';
export default function Pokestats(props) {
  return (
    <div>
      <div className={styles.pokedetail__stats}>
        <h2 className={styles.pokedetail__stats__title}>stats</h2>
          <div className={styles.pokedetail__stats__wrapper}>
            <div className={styles.pokedetail__stats__inner}>
              <Stat name={props.stats[5].name} value={props.stats[5].value}/>
              <Stat name={props.stats[3].name} value={props.stats[3].value}/>
              <Stat name={props.stats[1].name} value={props.stats[1].value}/>
            </div>
            <div className={styles.pokedetail__stats__inner}>
              <Stat name={props.stats[4].name} value={props.stats[4].value}/>
              <Stat name={props.stats[2].name} value={props.stats[2].value}/>
              <Stat name={props.stats[0].name} value={props.stats[0].value}/>
            </div>
          </div>
        </div>
    </div>
  )
}
