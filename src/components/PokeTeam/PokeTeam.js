import React from 'react'
import  { PokemonCard, DefaultCard } from '../PokemonCard/PokemonCard';
import styles from './Poketeam.module.scss';
export default function PokeTeam(props) {
  const getDefaultCards = pokemons => {
    let content = [];
    for (let i = 0; i < (6 - pokemons.length) ;i++) {
       
      content.push( <DefaultCard key={i} />);
    }
    return content;
  };
  const getpokemonCard = props.pokemons.map((pokemonName, index)=>{
    return (
      <PokemonCard key={index} PokemonName={pokemonName} className={styles.PokeCard}/>
       
    )
  })
  return (
    <div className={styles.PokeTeam__wrapper}>
      {getpokemonCard}
      {getDefaultCards(props.pokemons)}
    </div>
  )
}
