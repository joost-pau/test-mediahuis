import React from 'react'
import styles from './Stat.module.scss'
export default function Stat(props) {
  return (
    <p className={styles.stat__name}>
      {props.name}
      <span className={styles.stat__value}>{props.value}</span>
    </p>
  )
}
