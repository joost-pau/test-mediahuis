import React from 'react'
import { useQuery } from 'graphql-hooks'
import Downshift from 'downshift';
import styles from './PokemonList.module.scss';
import {POKELIST_QUERY} from '../../Querys/PokeListQuery'

export default function PokemonList(props) {
  const { loading, error, data } = useQuery(POKELIST_QUERY);
  if (loading) return 'Loading...'
  if (error) return 'Something Bad Happened'

  return (
    <>
    <Downshift 
    onChange={selection =>
      props.SelectPokemon(selection.name)
    }
    itemToString={item => (item ? item.name : '')}
     >
      {({getLabelProps, getInputProps, getMenuProps, getItemProps, highlightedIndex, selectedItem, inputValue})=> (

        <div className={styles.pokemonlist__wrapper}>
          <label {...getLabelProps()}>Select a Pokemon</label>
          <input {...getInputProps({
            placeholder: "TYPE TO FILTER"
          })}/>
          <ul {...getMenuProps()}>
          {data.Pokemons.filter(item => !inputValue || item.name.includes(inputValue)).map((item,index) => (
            <li{...getItemProps({
              key: item.id,
              index,
              item,
              style: {
                backgroundColor: highlightedIndex === index ? 'lightgray' : 'var(--blue)',
                fontWeight: selectedItem === item ? 'bold' : 'normal',
              },
            })}>{item.name}</li>
          ))}
        </ul>
        </div>
      )}

    </Downshift>    
    </>
  )
}
