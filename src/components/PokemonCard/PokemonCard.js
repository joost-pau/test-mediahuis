import React from 'react'
import { useQuery } from 'graphql-hooks'
import { POKEMON_CARD_QUERY } from '../../Querys/PokeCardQuery';
import styles from './PokeCard.module.scss'

export function PokemonCard(props) {
  const { loading, data } = useQuery(POKEMON_CARD_QUERY(props.PokemonName));
  if (loading) return 'Loading...'
  const pokemon = data.Pokemon;
  return (
    <article className={styles.PokemonCard} style={{backgroundColor: `var(--${pokemon.types[0].name}-type)`}}>
      <img src={pokemon.image} alt={pokemon.name}/>
      <p>{pokemon.name}</p>
      <ul>
      {pokemon.abilities.map((item,index)=>{
          return <li key={index}>{item.name} </li>
      })}
      </ul>
    </article>
  )
}


export function DefaultCard(){
  return(
    <div className={styles.default__member}><p>empty</p></div>
  )
}
