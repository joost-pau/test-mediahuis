import React from 'react'
import styles from './AbilitiesList.module.scss';
export default function AbilitiesList(props) {
  return (
    <div className={styles.pokedetail__abilities_wrapper}>
        <ul className={styles.pokeabilities__list}>
        {props.abilities.map((item,index)=>{
          return <li key={index}>{item.name} </li>
        })}
        </ul>
      </div>
  )
}
