import styles from './logo.module.scss';

export default function Logo(props) {
  return (
    <img
      {...props}
      alt="Pokémon"
      className={styles.logo}
      src={process.env.PUBLIC_URL + '/logo.png'}
    />
  );
}
