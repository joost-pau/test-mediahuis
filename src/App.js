import { GraphQLClient, ClientContext } from 'graphql-hooks';
import React, {useState} from 'react';
import PokemonList from './components/Pokelist/PokemonList';
import Container from './components/Container/Container';
import Logo from './components/Logo/Logo';
import PokeDetail from './components/PokeDetail/PokeDetail';
import styles from './App.module.scss'
import PokeTeam from './components/PokeTeam/PokeTeam';

const client = new GraphQLClient({
  url: process.env.REACT_APP_POKE_ENDPOINT,
});


export default function App() {
  const [detailNamePokemon, setDetailNamePokemon] = useState('');
  const [selectedPokemons, setSelectedPokemons] = useState([]);


  function addPokemon(){
    if (selectedPokemons.length < 6 ){
       setSelectedPokemons([...selectedPokemons, detailNamePokemon]);
    }else{
      alert('you have reached your maximum squad')
    }
  }
  return (
    <ClientContext.Provider value={client}>
      <Container>
        <Logo />
        <section className={styles.pokedex__top} >
          <PokemonList SelectPokemon={setDetailNamePokemon}/>
          {detailNamePokemon !== '' &&
            <PokeDetail  
              DetailPokemon={detailNamePokemon} 
              AddPokemon={addPokemon}
            />
          }
        </section>
        <section className={styles.pokedex__bottom}>
          <PokeTeam pokemons={selectedPokemons}/>
        </section>
      </Container>
    </ClientContext.Provider>
  );
}
